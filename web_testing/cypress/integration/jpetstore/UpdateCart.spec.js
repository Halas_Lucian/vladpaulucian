/// <reference types="cypress" />


describe('Remove from cart', () => {
  beforeEach(() => {
    cy.visit('https://petstore.octoperf.com/actions/Catalog.action?viewCategory=&categoryId=CATS')
  })

  it('Signs In', () => {
    cy.get('#Catalog td a').first().click()
    cy.get('#Catalog td a[class="Button"]').first().click();
    cy.get('input[name="EST-14"]').should('have.attr', 'value').and('eq', '1')
    cy.get('input[name="EST-14"]').clear();
    cy.get('input[name="EST-14"]').type('2');
    cy.get('input[name="updateCartQuantities"]').click();
    cy.get('input[name="EST-14"]').should('have.attr', 'value').and('eq', '2')
  })
})
