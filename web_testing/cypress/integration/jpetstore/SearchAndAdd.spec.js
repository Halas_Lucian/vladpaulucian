/// <reference types="cypress" />


describe('JPetStore Search and add', () => {
    beforeEach(() => {
        cy.visit('https://petstore.octoperf.com/actions/Catalog.action')
    })

    it('Signs In', () => {
        cy.get('input[name="keyword"]').type('fin')
        cy.get('input[name="searchProducts"]').click()
        cy.get('#Catalog').contains('Fin')
        cy.get('#Catalog a').should('have.attr', 'href')
        cy.get('#Catalog td > a:nth-child(1)').click()
        cy.get('#Catalog').contains('Fin')
        cy.get('a.Button').should('have.attr', 'href')
        cy.get('a.Button').click()
        cy.get('input[name="EST-19"]').should('have.attr', 'value').and('eq', '1')
    })
})
