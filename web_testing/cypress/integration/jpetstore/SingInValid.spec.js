/// <reference types="cypress" />

const utils = require('./Utils.json'); 

describe('JPetStore Sign In Valid', () => {
    beforeEach(() => {
        cy.visit('https://petstore.octoperf.com/actions/Catalog.action')
    })

    it('Signs In', () => {
        cy.get('#MenuContent > a:nth-child(3)').should('have.attr', 'href').and('include', 'signonForm')
        cy.get('#MenuContent > a:nth-child(3)').click()
        cy.get('input[name="username"]').type(utils.validUsername)
        cy.get('input[name="password"]').clear()
        cy.get('input[name="password"]').type(utils.validPassword)
        cy.get('input[name="signon"]').click()
        cy.get('#WelcomeContent').contains('Welcome')
        cy.get('#MenuContent > a:nth-child(3)').should('have.attr', 'href').and('include', 'signoff')
    })
})
