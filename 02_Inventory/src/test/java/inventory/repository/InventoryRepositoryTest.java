package inventory.repository;

import inventory.model.InhousePart;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class InventoryRepositoryTest {

    @Mock
    PartsInventory partsInventory;
    @Mock
    ProductsInventory productsInventory;

    @InjectMocks
    InventoryRepository repo;

    private InhousePart part = new InhousePart(1,"name",10,10,10,100,1);


    @Test
    @DisplayName("lookup part successfully")
    public void lookupSuccessfully(){
        when(partsInventory.lookup("name")).thenReturn(part);
        assertEquals(repo.lookupPart(part.getName()).getPartId(), part.getPartId());
    }
    @Test
    @DisplayName("lookup part unsuccessfully")
    public void lookupUnsuccessfully(){
        when(partsInventory.lookup(anyString())).thenReturn(null);
        assertNull(repo.lookupPart(part.getName()));
    }


}