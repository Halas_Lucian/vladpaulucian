package inventory.integration.topDown;
import inventory.model.InhousePart;
import inventory.model.Part;
import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import inventory.service.ServiceException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class EIntegrationTests {
    static InventoryRepository inventoryRepository = new InventoryRepository();
    static InventoryService inventoryService = new InventoryService(inventoryRepository);

    @BeforeEach
    void setup() {
        inventoryRepository.clearParts();
    }

    @AfterEach
    void tearDown() {
        inventoryRepository.clearParts();
    }

    @Test
    @DisplayName("E integration add part successful")
    void E_integration_add_part_successful() {
        InhousePart part = new InhousePart(1, "name", 10, 10, 10, 100, 1);

        try {
            inventoryService.addInhousePart(part);
        } catch (ServiceException ex) {
            assert false;
        }

        assertEquals(1, inventoryRepository.getAllParts().size());
    }

    @Test
    @DisplayName("E integration add part unsuccessful")
    void E_integration_add_part_unsuccessful() {
        InhousePart part = new InhousePart(1, "", 10, 10, 10, 100, 1);

        Throwable exception = assertThrows(ServiceException.class,
                () -> inventoryService.addInhousePart(part)
        );

        assertEquals("A name has not been entered. A name must have between 2 and 100 characters. ", exception.getMessage());
    }

    @Test
    @DisplayName("E integration lookup successful")
    void E_integration_lookup_successful() throws ServiceException {
        InhousePart part = new InhousePart(1, "name", 10, 10, 10, 100, 1);
        inventoryService.addInhousePart(part);

        Part result = inventoryService.lookupPart("name");

        assertEquals(1,inventoryRepository.getAllParts().size());
        assertEquals(part.getName(), result.getName());
    }

    @Test
    @DisplayName("E integration lookup unsuccessful")
    void E_integration_lookup_unsuccessful() {
        Part result = inventoryService.lookupPart("name");
        assertEquals(0,inventoryRepository.getAllParts().size());
        assertNull(result);
    }

}

