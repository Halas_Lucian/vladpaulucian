package inventory.service;

import inventory.model.Product;
import inventory.repository.ProductsInventory;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ProductsInventoryLookupTest {

    private static final ProductsInventory inventory = new ProductsInventory();


        @BeforeAll
        static void setup() {
            inventory.add(new Product(1, "Apa", 10.0, 10, 1, 50, null));
            inventory.add(new Product(2, "Lapte", 10.0, 10, 1, 50, null));
            inventory.add(new Product(3, "Orez", 10.0, 10, 1, 50, null));
        }

        @AfterAll
        static void tearDown() {
            inventory.remove(new Product(1, "Apa", 10.0, 10, 1, 50, null));
            inventory.remove(new Product(2, "Lapte", 10.0, 10, 1, 50, null));
            inventory.remove(new Product(3, "Orez", 10.0, 10, 1, 50, null));
        }

        @Test
        void Test_F02_TC01() {
            Product p = new Product(0, null, 0, 0, 0, 0, null);
            Product result = inventory.lookup(null);
            assertEquals(p.getProductId(), result.getProductId());
            assertEquals(p.getName(), result.getName());
        }

        @Test
        void F02_TC02() {
            Product p = new Product(0, null, 0, 0, 0, 0, null);
            Product result = inventory.lookup("");
            assertEquals(p.getProductId(), result.getProductId());
            assertEquals(p.getName(), result.getName());
        }

        @Test
        void F02_TC03() {
            Product p = new Product(1, "Apa", 10, 10, 1, 50, null);
            Product result = inventory.lookup("A");
            assertEquals(p.getProductId(), result.getProductId());
            assertEquals(p.getName(), result.getName());
        }

        @Test
        void F02_TC04() {
            Product p = new Product(1, "Apa", 10, 10, 1, 50, null);
            Product result = inventory.lookup("1");
            assertEquals(p.getProductId(), result.getProductId());
            assertEquals(p.getName(), result.getName());
        }

        @Test
        void F02_TC05() {
            Product p = new Product(2, "Lapte", 10, 10, 1, 50, null);
            Product result = inventory.lookup("2");
            assertEquals(p.getProductId(), result.getProductId());
            assertEquals(p.getName(), result.getName());
        }

        @Test
        void F02_TC06() {
            ObservableList<Product> list = FXCollections.observableArrayList();
            list.add(new Product(1, "Apa", 10.0, 10, 1, 50, null));
            inventory.setItems(list);
            Product expectedResult = new Product(0, null, 0, 0, 0, 0, null);
            Product actualResult = inventory.lookup("X");
            assertEquals(expectedResult.getProductId(), actualResult.getProductId());
            assertEquals(expectedResult.getName(), actualResult.getName());
            assertEquals(expectedResult.getPrice(), actualResult.getPrice());
            assertEquals(expectedResult.getInStock(), actualResult.getInStock());
            assertEquals(expectedResult.getMin(), actualResult.getMin());
            assertEquals(expectedResult.getMax(), actualResult.getMax());
        }

        @Test
        void F02_TC07() {
            inventory.setItems( FXCollections.observableArrayList());
            Product expectedResult = new Product(0, null, 0, 0, 0, 0, null);
            Product actualResult = inventory.lookup("X");
            assertEquals(expectedResult.getProductId(), actualResult.getProductId());
            assertEquals(expectedResult.getName(), actualResult.getName());
            assertEquals(expectedResult.getPrice(), actualResult.getPrice());
            assertEquals(expectedResult.getInStock(), actualResult.getInStock());
            assertEquals(expectedResult.getMin(), actualResult.getMin());
            assertEquals(expectedResult.getMax(), actualResult.getMax());
        }

}
