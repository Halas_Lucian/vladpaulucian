package inventory.service;

import inventory.model.InhousePart;
import inventory.repository.InventoryRepository;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class InventoryServiceMockTest {

    @Mock
    InventoryRepository inventoryRepository;

    @InjectMocks
    InventoryService inventoryService;

    private InhousePart part = new InhousePart(1,"name",10,10,10,100,1);


    @Test
    @DisplayName("lookup test successfully")
    public void lookupSuccessfully(){
        when(inventoryRepository.lookupPart("name")).thenReturn(part);
        assertEquals(inventoryService.lookupPart(part.getName()).getPartId(),1);
    }
    @Test
    @DisplayName("lookup test unsuccessfully")
    public void lookupUnsuccessfully(){
        when(inventoryRepository.lookupPart(anyString())).thenReturn(null);
        assertEquals(inventoryService.lookupPart(part.getName()),null);
    }

}