package inventory.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InhousePartTest {

    private InhousePart part = new InhousePart(1,"name",10,10,10,100,1);

    @Test
    public void gettersTest(){
        assertEquals(part.getPartId(),1);
        assertEquals(part.getName(),"name");
        assertEquals(part.getPrice(),10);
        assertEquals(part.getMin(),10);
        assertEquals(part.getMax(),100);
        assertEquals(part.getMachineId(),1);
    }
    @Test
    public void settersTest(){
        part.setPartId(2);
        assertEquals(part.getPartId(),2);
        part.setName("setter");
        assertEquals(part.getName(),"setter");
        part.setPrice(15);
        assertEquals(part.getPrice(),15);
        part.setMin(5);
        assertEquals(part.getMin(),5);
        part.setMax(50);
        assertEquals(part.getMax(),50);
        part.setMachineId(2);
        assertEquals(part.getMachineId(),2);
    }

}