package inventory.repository;

import javafx.collections.ObservableList;

public interface InventoryInterface<E> {
    void add( E item);

    void remove( E item );

     E lookup(String searchItem);

    void update(int index,E item );

    ObservableList<E> getItems();

    void setItems(ObservableList<E> list);

    void setItemId(int id);
    int getItemId();
}
